from flask import Response, jsonify
from flask_restful import Resource, abort

agents = {
    '9342a55b-1d2f-4c40-93bc-812f7149983f':{'hostname': 'DESKTOP-A', 'details': '', 'status': 'online'},
    'fa705f79-c7fe-442f-99ca-6a330219ffca':{'hostname': 'DESKTOP-B', 'details': '', 'status': 'online'},
    'fe22b307-96ae-4686-bee1-942839a170de':{'hostname': 'DESKTOP-C', 'details': '', 'status': 'online'},
    '94490c30-b0c8-4abf-97d3-f562f930ccf9':{'hostname': 'DESKTOP-D', 'details': '', 'status': 'online'},
    '7c95e018-467f-442c-b6f4-e0cd6bc56f5e':{'hostname': 'DESKTOP-E', 'details': '', 'status': 'online'},
    '27984c00-195c-4796-b8f3-c5303c96b745':{'hostname': 'DESKTOP-F', 'details': '', 'status': 'online'},
    '7f8176c3-64f9-4e89-8389-911ca47f5838':{'hostname': 'DESKTOP-G', 'details': '', 'status': 'offline'},
    'ebb0ad63-0631-43ad-a676-2eec49dda0e9':{'hostname': 'DESKTOP-H', 'details': '', 'status': 'offline'},
    'becd34f5-59c8-4052-becf-03c5172c5039':{'hostname': 'DESKTOP-I', 'details': 'Process frozen', 'status': 'error'},
    '0ed14176-0e94-4ce5-b83a-316f1d515955':{'hostname': 'DESKTOP-J', 'details': 'Not enough logging space', 'status': 'error'}
}

def check_exists(id: str) -> None:
    if id not in agents:
        abort(404, message="Agent {} doesn't exist".format(id))

class Agents(Resource):
    """Manage all agents
    """
    def get(self) -> Response:
        """Get the information about all agents/

        Returns:
            Response: Dictionary of agents with their id's stored in the dictionary keys.
        """
        return jsonify(agents)


class Agent(Resource):
    """Perform operations on all agents
    """
    def get(self, agent_id: str) -> Response:
        """Obtain information about single agent.

        Args:
            agent_id (str): GUID of a desigred agent.

        Returns:
            Response: Object containing information about desigred agent.
                #TODO - detailed description of possible responses
        """
        check_exists(id)
        return jsonify(agents[id])

    def put(self, agent_id: str) -> Response:
        """Update information about single agent.

        Args:
            agent_id (str): GUID of a desigred agent.

        Returns:
            Response: 
                #TODO
        """
        check_exists(id)
        return jsonify(agents[id])