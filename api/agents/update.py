from flask import Response, jsonify
from flask_restful import Resource, abort


class AgentsUpdate(Resource):
    
    def get(self, agent_id: str) -> Response:
        """[summary]
            #TODO
        Args:
            agent_id (str): [description]

        Returns:
            Response: [description]
        """
        return jsonify({'latest': '0.0.5', 'status': 'uptodate'})

    def put(self, agent_id: str) -> Response:
        """[summary]
            #TODO
        Args:
            agent_id (str): [description]

        Returns:
            Response: [description]
        """
        return jsonify({'latest': '0.0.5','status': 'update in progress'})

    
class AgentsUpdateAll(Resource):
    def get(self)  -> Response:
        """[summary]
            #TODO
        Returns:
            Response: [description]
        """
        return jsonify({'latest': '0.0.5', 'status': "All agents uptodate"})

    def put(self) -> Response:
        """[summary]
            #TODO
        Returns:
            Response: [description]
        """
        return jsonify({'status': 'update in progress'})