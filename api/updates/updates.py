from flask import Response, jsonify
from flask_restful import Resource, abort

update_list = {
        '9342a55b-1d2f-4c40-93bc-812f7149983f': {'no': '0.0.1', 'notes': 'Changed something'},
        'fa705f79-c7fe-442f-99ca-6a330219ffca': {'no': '0.0.2', 'notes': 'Changed something'},
        'fe22b307-96ae-4686-bee1-942839a170de': {'no': '0.0.3', 'notes': 'Changed something'},
        '94490c30-b0c8-4abf-97d3-f562f930ccf9': {'no': '0.0.4', 'notes': 'Changed something'},
        '7c95e018-467f-442c-b6f4-e0cd6bc56f5e': {'no': '0.0.5', 'notes': 'Changed something'}
    }

class Updates(Resource):
    
    def get(self) -> Response:
        """[summary]
            #TODO
        Returns:
            Response: [description]
        """
        return jsonify(update_list)