# flask packages

from api.general.user import GeneralUser
from api.configs.configs import Configs
from api.license.license import License
from api.agents.config import AgentsConfig, AgentsConfigs
from api.agents.status import AgentsStatus
from api.agents.update import AgentsUpdate, AgentsUpdateAll
from api.agents.agents import Agent, Agents

from api.updates.updates import Updates


# from api.agents.update import Update, UpdateList, UpdateStatus

from flask_restful import Api

# project resources
# from api.authentication import SignUpApi, LoginApi
# from api.user import UsersApi, UserApi
# from api.meal import MealsApi, MealApi


def create_routes(api: Api):
    

    # agents
    api.add_resource(Agents, '/agents/')
    api.add_resource(AgentsStatus, '/agents/status')
    api.add_resource(Agent, '/agents/<string:agent_id>')
    api.add_resource(AgentsUpdateAll, '/agents/update')
    api.add_resource(AgentsUpdate, '/agents/<string:agent_id>/update')
    api.add_resource(AgentsConfigs, '/agents/<string:agent_id>/config/history')
    api.add_resource(AgentsConfig, '/agents/<string:agent_id>/config')

    api.add_resource(Updates, '/updates')
    api.add_resource(Configs, '/configs')
    api.add_resource(License, '/license')

    api.add_resource(GeneralUser, "/general/user")